import 'package:flutter/material.dart';
import 'package:fluttericon/font_awesome_icons.dart';
import 'package:fluttericon/font_awesome5_icons.dart';
import 'package:flutter_travel_ui/ui/widgets/cardExclusiveHotels.dart';
import 'package:flutter_travel_ui/ui/widgets/cardTopDestination.dart';
import 'package:flutter_travel_ui/ui/widgets/titleTopic.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _currentIconSelected = 0;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              buildHeader(context),
              buildTopDestination(context),
              buildExclusiveHotels(),
            ],
          ),
        ),
      ),
    );
  }

  Container buildHeader(BuildContext context) => Container(
        padding: const EdgeInsets.only(top: 20, right: 15.0, left: 15.0),
        child: Column(
          children: <Widget>[
            Text(
              'What you would like to find?',
              style: TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 25.0, bottom: 25.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [FontAwesome.flight, FontAwesome5.car_side, FontAwesome5.parachute_box, FontAwesome5.motorcycle]
                    .asMap()
                    .map((key, value) => MapEntry(
                        key,
                        Ink(
                          padding: EdgeInsets.all(0),
                          // height: 100,
                          decoration: ShapeDecoration(
                            color: _currentIconSelected == key ? Color.fromARGB(255, 217, 235, 241) : Colors.grey[300],
                            shape: CircleBorder(),
                          ),
                          child: IconButton(
                            padding: EdgeInsets.all(22),
                            icon: Icon(value),
                            iconSize: 27.0,
                            color: _currentIconSelected == key ? Theme.of(context).textSelectionHandleColor : Colors.grey[400],
                            onPressed: () => {
                              setState(() {
                                _currentIconSelected = key;
                              })
                            },
                          ),
                        )))
                    .values
                    .toList(),
              ),
            ),
          ],
        ),
      );

  Column buildTopDestination(BuildContext context) {
    return Column(
      children: <Widget>[
        TitleTopic(
          title: "Top Destinations",
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              CardTopDestinationWidget(
                city: "Venice",
                country: "Italy",
                nbActivities: 125,
                description: "Enjoy best trips from top travel agencies at best prices",
                imageUrl: "./assets/images/venice.jpg",
              ),
              CardTopDestinationWidget(
                city: "Paris",
                country: "France",
                nbActivities: 201,
                description: "Enjoy best trips from top travel agencies at best prices",
                imageUrl: "./assets/images/paris.jpg",
              )
            ],
          ),
        ),
      ],
    );
  }

  Padding buildExclusiveHotels() => Padding(
      padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
      child: Column(
        children: <Widget>[
          TitleTopic(
            title: "Exclusive Hotels",
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                CardExclusiveHotelsWidget(
                  hotelName: "Hotel 0",
                  street: "Daintree Street",
                  price: "175",
                  city: "Venice",
                  country: "Italy",
                  imageUrl: "./assets/images/hotel0.jpg",
                ),
                CardExclusiveHotelsWidget(
                  hotelName: "Hotel 1",
                  street: "Drapper Street",
                  price: "50",
                  city: "Sao Polo",
                  country: "Brazil",
                  imageUrl: "./assets/images/hotel1.jpg",
                )
              ],
            ),
          )
        ],
      ));
}
