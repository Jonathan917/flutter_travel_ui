import 'package:flutter/material.dart';
import 'package:flutter_travel_ui/ui/screen/home.dart';
import 'package:flutter_travel_ui/ui/screen/kart.dart';
import 'package:flutter_travel_ui/ui/screen/profile.dart';

class BottomNavScreen extends StatefulWidget {
  @override
  _BottomNavScreenState createState() => _BottomNavScreenState();
}

class _BottomNavScreenState extends State<BottomNavScreen> {
  final List _screen = [HomeScreen(), KartScreen(), ProfileScreen()];
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _screen[_currentIndex],
        backgroundColor: Theme.of(context).backgroundColor,
        bottomNavigationBar: BottomNavigationBar(
            backgroundColor: Color.fromARGB(255, 245, 245, 245),
            currentIndex: _currentIndex,
            type: BottomNavigationBarType.fixed,
            elevation: 0,
            iconSize: 25.0,
            selectedItemColor: Theme.of(context).textSelectionHandleColor,
            unselectedItemColor: Colors.grey[400],
            showSelectedLabels: false,
            showUnselectedLabels: false,
            onTap: (index) => setState(() => _currentIndex = index),
            items: [Icons.search, Icons.card_travel, Icons.face]
                .asMap()
                .map((key, value) => MapEntry(
                    key,
                    BottomNavigationBarItem(
                        label: '',
                        icon: Container(
                          child: Icon(value),
                        ))))
                .values
                .toList()));
  }
}
