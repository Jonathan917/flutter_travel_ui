import 'package:flutter/material.dart';
import 'package:flutter_travel_ui/ui/widgets/cardActivity.dart';
import 'package:fluttericon/font_awesome5_icons.dart';
import 'package:fluttericon/typicons_icons.dart';

class DetailDestinationScreen extends StatelessWidget {
  final String _country;
  final String _city;
  final String _imageUrl;

  const DetailDestinationScreen({
    Key key,
    @required String country,
    @required String city,
    @required String imageUrl,
  })  : _country = country,
        _city = city,
        _imageUrl = imageUrl,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            buildHeader(context),
            buildActivity(),
          ],
        ),
      ),
    );
  }

  Column buildActivity() {
    return Column(
      children: [
        CardActivity(
          title: "St.Marks Basilica",
          activityType: "Sightseeing Tour",
          price: 30,
          date: ["9:00 am", "11:00 am"],
          imageURL: "./assets/images/stmarksbasilica.jpg",
          rate: 5,
        ),
        CardActivity(
          title: "Walking Tour gondola ride",
          activityType: "Sightseeing Tour",
          price: 210,
          date: ["7:00 am", "8:00 am"],
          imageURL: "./assets/images/gondola.jpg",
          rate: 4,
        ),
        CardActivity(
          title: "Murano and Burano Tour",
          activityType: "Sightseeing Tour",
          price: 125,
          date: ["12:00 am"],
          imageURL: "./assets/images/murano.jpg",
          rate: 5,
        ),
      ],
    );
  }

  Padding buildHeader(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20.0),
      child: Stack(
        children: <Widget>[
          Container(
            height: 450,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              color: Theme.of(context).backgroundColor,
              image: DecorationImage(image: AssetImage(_imageUrl), fit: BoxFit.fitHeight),
              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(40), bottomRight: Radius.circular(40)),
            ),
          ),
          Positioned(
              top: 40,
              left: 10,
              child: IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  size: 33,
                ),
                onPressed: () => {Navigator.pop(context)},
              )),
          Positioned(
              top: 40,
              right: 70,
              child: IconButton(
                icon: Icon(
                  Icons.search,
                  size: 31,
                ),
                onPressed: () => {},
              )),
          Positioned(
              top: 40,
              right: 20,
              child: IconButton(
                icon: Icon(
                  Icons.calendar_today,
                  size: 28,
                ),
                onPressed: () => {},
              )),
          Positioned(
              bottom: 60,
              left: 20,
              child: Text(_city,
                  style: TextStyle(
                    fontSize: 26,
                    color: Colors.grey[100],
                  ))),
          Positioned(
              bottom: 40,
              left: 20,
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Icon(
                      Typicons.direction,
                      color: Colors.grey[300],
                      size: 15.0,
                    ),
                  ),
                  Text(
                    _country,
                    style: TextStyle(fontSize: 17.0, color: Colors.grey[300], fontWeight: FontWeight.w700),
                  ),
                ],
              )),
          Positioned(
              bottom: 40,
              right: 20,
              child: Icon(
                FontAwesome5.map_marker_alt,
                color: Colors.grey[300],
              )),
        ],
      ),
    );
  }
}
