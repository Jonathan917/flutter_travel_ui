import 'package:flutter/material.dart';
import 'package:flutter_travel_ui/ui/screen/bottomNav.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: ThemeData(
          primaryColor: Color.fromARGB(255, 226, 243, 246),
          // primaryColorLight: Color.fromARGB(255, 153, 159, 191),
          // accentColor: Color.fromARGB(255, 226, 243, 246),
          backgroundColor: Color.fromARGB(255, 239, 240, 244),
          // buttonColor: Color.fromARGB(255, 76, 121, 255),
          // accentColor: Color.fromARGB(255, 255, 76, 88),
          // textSelectionColor: Colors.white,
          textSelectionHandleColor: Color.fromARGB(255, 88, 196, 212)),
      routes: {},
      // AuthBlocProvider(child: RegisterScreen())},
      // onUnknownRoute: (RouteSettings setting) {
      //   return new MaterialPageRoute(builder: (context) => BottomNavScreen());
      // },
      home: BottomNavScreen(),
    );
  }
}
