import 'package:flutter/material.dart';

class TitleTopic extends StatelessWidget {
  final String _title;

  const TitleTopic({Key key, @required String title})
      : _title = title,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 15.0, left: 15.0, bottom: 20.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            _title,
            style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.w700),
          ),
          GestureDetector(
            onTap: () => {},
            child: Text(
              "See All",
              style: TextStyle(fontSize: 18.0, color: Theme.of(context).textSelectionHandleColor, fontWeight: FontWeight.w600),
            ),
          )
        ],
      ),
    );
  }
}
