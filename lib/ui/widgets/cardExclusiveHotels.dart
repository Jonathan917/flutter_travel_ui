import 'package:flutter/material.dart';
import 'package:fluttericon/typicons_icons.dart';

class CardExclusiveHotelsWidget extends StatelessWidget {
  final String _hotelName;
  final String _street;
  final String _price;
  final String _country;
  final String _city;
  final String _imageUrl;

  const CardExclusiveHotelsWidget({
    Key key,
    @required String hotelName,
    @required String street,
    @required String price,
    @required String country,
    @required String city,
    @required String imageUrl,
  })  : _hotelName = hotelName,
        _street = street,
        _price = price,
        _country = country,
        _city = city,
        _imageUrl = imageUrl,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 15),
      child: Stack(
        fit: StackFit.loose,
        overflow: Overflow.clip,
        children: <Widget>[
          Container(
            height: 300,
            width: 260,
          ),
          Positioned(
            left: 0,
            bottom: 0,
            child: Container(
              height: 120,
              width: 260,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                color: Colors.white,
              ),
              padding: EdgeInsets.only(bottom: 10, right: 10, left: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text(_hotelName, style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600)),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5.0),
                    child: Text(
                      _street,
                      style: TextStyle(fontSize: 15.0, color: Colors.grey[400]),
                    ),
                  ),
                  Text(
                    "\$$_price/night",
                    style: TextStyle(fontSize: 19.0, color: Colors.black),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: 0,
            left: 12,
            child: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: Image.asset(
                  _imageUrl,
                  height: 202,
                  width: 236,
                  fit: BoxFit.fill,
                )),
          ),
          Positioned(
            top: 140.0,
            left: 20.0,
            child: Text(
              _city,
              style: TextStyle(fontSize: 24.0, color: Colors.grey[100], fontWeight: FontWeight.w700),
            ),
          ),
          Positioned(
            top: 170.0,
            left: 20.0,
            child: Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Icon(
                    Typicons.direction,
                    color: Colors.grey[300],
                    size: 15.0,
                  ),
                ),
                Text(
                  _country,
                  style: TextStyle(fontSize: 17.0, color: Colors.grey[300], fontWeight: FontWeight.w700),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
