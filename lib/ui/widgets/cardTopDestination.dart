import 'package:flutter/material.dart';
import 'package:fluttericon/typicons_icons.dart';
import 'package:flutter_travel_ui/ui/screen/detailDestination.dart';

class CardTopDestinationWidget extends StatelessWidget {
  final int _nbActivities;
  final String _country;
  final String _city;
  final String _description;
  final String _imageUrl;

  const CardTopDestinationWidget({
    Key key,
    @required int nbActivities,
    @required String country,
    @required String city,
    @required String description,
    @required String imageUrl,
  })  : _nbActivities = nbActivities,
        _country = country,
        _city = city,
        _description = description,
        _imageUrl = imageUrl,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DetailDestinationScreen(
                      imageUrl: _imageUrl,
                      city: _city,
                      country: _country,
                    )))
      },
      child: Padding(
        padding: const EdgeInsets.only(left: 15),
        child: Stack(
          fit: StackFit.loose,
          overflow: Overflow.clip,
          children: <Widget>[
            Container(
              height: 300,
              width: 226,
            ),
            Positioned(
              left: 0,
              bottom: 0,
              child: Container(
                height: 120,
                width: 226,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                ),
                padding: EdgeInsets.only(bottom: 10, right: 10, left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: Text("$_nbActivities activities", style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600)),
                    ),
                    Text(
                      _description,
                      style: TextStyle(fontSize: 15.0, color: Colors.grey[400]),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              top: 0,
              left: 12,
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(20.0),
                  child: Image.asset(
                    _imageUrl,
                    height: 202,
                    width: 202,
                    fit: BoxFit.fill,
                  )),
            ),
            Positioned(
              top: 140.0,
              left: 20.0,
              child: Text(
                _city,
                style: TextStyle(fontSize: 24.0, color: Colors.grey[100], fontWeight: FontWeight.w700),
              ),
            ),
            Positioned(
              top: 170.0,
              left: 20.0,
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Icon(
                      Typicons.direction,
                      color: Colors.grey[300],
                      size: 15.0,
                    ),
                  ),
                  Text(
                    _country,
                    style: TextStyle(fontSize: 17.0, color: Colors.grey[300], fontWeight: FontWeight.w700),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
