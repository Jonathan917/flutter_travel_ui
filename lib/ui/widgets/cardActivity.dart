import 'package:flutter/material.dart';

class CardActivity extends StatelessWidget {
  final String _title;
  final int _price;
  final String _activityType;
  final List<String> _date;
  final String _imageURL;
  final int _rate;

  const CardActivity({Key key, @required String title, @required int price, @required String activityType, @required List<String> date, @required String imageURL, @required int rate})
      : _title = title,
        _price = price,
        _activityType = activityType,
        _date = date,
        _imageURL = imageURL,
        _rate = rate,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 15.0, right: 15.0, left: 40),
      child: Stack(
        fit: StackFit.loose,
        overflow: Overflow.visible,
        children: [
          Container(
            height: 200,
            padding: EdgeInsets.only(left: 120.0, right: 10.0, top: 25.0),
            decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(10)), color: Colors.white),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 100,
                      child: Text(
                        _title,
                        softWrap: true,
                        maxLines: 2,
                        style: TextStyle(fontSize: 21.0, fontWeight: FontWeight.w700),
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "\$$_price",
                          style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w700),
                        ),
                        Text(
                          "per pax",
                          style: TextStyle(fontSize: 15.0, color: Colors.grey[400]),
                        )
                      ],
                    )
                  ],
                ),
                Text(
                  _activityType,
                  style: TextStyle(fontSize: 15.0, color: Colors.grey[400]),
                ),
                Row(
                  children: List.generate(
                      _rate,
                      (index) => Icon(
                            Icons.star,
                            size: 13.0,
                            color: Colors.yellow[700],
                          )),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15.0),
                  child: Row(
                    children: _date
                        .map((e) => Padding(
                              padding: EdgeInsets.only(right: 5),
                              child: Chip(
                                label: Text(e),
                                padding: EdgeInsets.only(right: 5),
                                backgroundColor: Colors.lightBlue[50],
                              ),
                            ))
                        .toList(),
                  ),
                )
              ],
            ),
          ),
          Positioned(
            top: 15,
            left: -25,
            child: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: Image.asset(
                  _imageURL,
                  height: 170,
                  width: 130,
                  fit: BoxFit.fill,
                )),
          ),
        ],
      ),
    );
  }
}
